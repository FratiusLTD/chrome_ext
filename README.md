# chrome_ext

Extension to store emails into Excel sheet with columns as Sender, Subject, Message (and others)
Extension will be written in js adn Python

# Step 1

Establish connection to gmail (as it could be the easiest one) via IMAP 

Since Chrome Extensions cannot directly make TCP/IP connections (required for IMAP), 
i'll need a server-side component that the extension can communicate with via HTTPS.

https://developer.mozilla.org/en-US/docs/Learn/Server-side/First_steps/Client-Server_overview

