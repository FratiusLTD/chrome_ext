import imaplib
import email
import datetime
import pandas as pd
import json
import getpass
import bs4
from bs4 import BeautifulSoup 
from email.header import decode_header
from pprint import pprint


# Connect to the IMAP server
mail = imaplib.IMAP4('imap.one.com')
# mail = imaplib.IMAP4_SSL('imap.gmail.com', 993)

#TODO: If we are using different emails we will need to change the name of the file to match the email

print('\n Please provide a file to store the emails in. \nIf you do not provide a file, the emails will be stored in a file called emails.xlsx. \n')
file_name = input('Enter the name of the file to store the emails in: ').strip()

if file_name == '':
    file_name = 'emails.xlsx'
elif file_name.split('.')[-1] != 'xlsx':
    file_name = file_name + '.xlsx'
    

# Ask the user for their email and password
print('Please enter your email and password to access your ONE mailbox. \n')
userEmail = input('Enter your email: ').strip()
keyWord = getpass.getpass(f'Enter your password for {userEmail}: ')




try:
    # Login to the server
    mail.login(userEmail, keyWord)
except Exception as e:
    print(f'An error occurred: {e}')
    print('Please check your email and password and try again.')
    exit()

# Select the mailbox you want to work with
mail.select('INBOX')

# Perform operations on the mailbox
# Display the first 5 messages in the mailbox
status, response = mail.search(None, 'ALL')

# Initialize a dictionary to store the emails
emails = {}

# Check the number of total emails in the mailbox
if status == 'OK':
    email_ids = response[0].split()
    no_of_emails = len(email_ids)
    print(f'\nProcessing {no_of_emails} emails...\n')


# Calculate the time it takes to process the emails
start = datetime.datetime.now()


def add_text_plain_to_list(part, list_to_append):
    message_body = part.get_payload(decode=True)
    message_body =  message_body.decode('utf-8', errors='replace')
    list_to_append.append(message_body)
                        
def add_text_html_to_list(part, list_to_append):
    soupe = BeautifulSoup(part.get_payload(decode=True), 'html.parser')
    message_body = soupe.get_text(separator=' ')
    message_body = " ".join(message_body.split())
    list_to_append.append(message_body)

# TODO: the first email is not being stored in the file
# try:
# Fetch emails by IDs
if status == 'OK':
    email_ids = response[0].split()
    
    # get the numbers of emails already stored in the file_name file
    try:
        df = pd.read_excel(file_name, index_col=0)
        no_emails_stored = df['email_id'].shape[0]
        emails_stored = df['email_id'].tolist()

    except Exception as e:
        print(f'An error occurred: {e}')
        df = pd.DataFrame()
        no_emails_stored = 0
        emails_stored = []
    print(f'You have {no_emails_stored} emails stored in the file.')

    total_emails = len(email_ids)
    
    print(f'You have {total_emails} emails stored in the mailbox.\n')
        
    # if the number of emails stored in the file is less than the number of emails in the mailbox
    # then we fetch the emails from the mailbox
    if no_emails_stored < total_emails:
        print(f'You have {no_emails_stored} emails stored in the file. Fetching the remaining {total_emails - no_emails_stored} emails...')
        email_ids = email_ids[no_emails_stored:]
    elif no_emails_stored == total_emails:
        print('All emails have been stored in the file.')
        exit()
    else:
        print('You have more emails stored in the file than in the mailbox. Please check the file and try again.')
        print('Would you like to rewrite the file? Y \ N:\n')
        print('That will be a nice feature for the future.')
        exit()
        # rewrite = input('Enter yes or no: ').strip().lower()
        # if rewrite[0] == 'n':
        #     exit()
        # else:
        #     print('Rewriting the file...')
        #     df = pd.DataFrame()
        #     emails_stored = []
   
    for e_id in email_ids:
        if e_id in emails_stored:
            continue

        # Fetch the email's bytes
        status, email_data = mail.fetch(e_id, '(RFC822)')
        if status == 'OK':
            # Parse the email's bytes into a Python object
            email_message = email.message_from_bytes(email_data[0][1])
            
            # Decode the email subject
            subject = decode_header(email_message['Subject'])[0][0]
            
            if isinstance(subject, bytes):
                subject = subject.decode(errors='replace')
            else:
                subject = subject
            
            # Extract the date of the email
            date = email_message['Date']
            
            if date is None:
                date = email_message['Received']
                date = date.split(';')
                date = date[1]
                date = date.strip()
            elif date is not None:
                # Convert the date to a datetime object
                date = email.utils.parsedate_to_datetime(date)
            else:
                date = 'No date was found'
            
            if isinstance(date, datetime.datetime):
                # remove the timezone
                date = date.replace(tzinfo=None)
            else:
                date = 'No date was found'
            
                
            # Get the sender
            from_ = email_message['From']

        # Initialize message body
        # message_body_total = {}
        message_body = None
        
        message_body_list = []
        
        # print(f' Email message raw is {email_message.get_content_type()}')
        
        # TODO: Investigate why all emails are multipart
        # We have very few emails that are not multipart
        
        """
        for part in email_message.walk():
            cdispo = str(part.get('Content-Disposition'))
            ctype = part.get_content_type()
            
            if ctype == 'text/html' and 'attachment' not in cdispo:
                add_text_html_to_list(part, message_body_list)             
                # add_text_plain_to_list(part, message_body_list)
            
            if part.get_content_maintype() == 'multipart':
                for part in email_message.walk():
                    if ctype == 'text/html' and 'attachment' not in cdispo:
                        add_text_html_to_list(part, message_body_list)            
                        # add_text_plain_to_list(part, message_body_list)
                        
            if message_body_list == []:
                if ctype == 'text/plain' and 'attachment' not in cdispo:
                    add_text_plain_to_list(part, message_body_list)
            
        """
        
        if message_body_list != []:
            # TODO: the message body is displayed with characters like \n, \t, \r, etc. --> Needs to be cleaned
            emails[int(e_id)] = {
                'date': date,
                'from': from_,
                'subject': subject,
                'email_id': e_id,
                # 'message': message_body_list
            }
            
        else:
            emails[int(e_id)] = {
                'date': date,
                'from': from_,
                'subject': subject,
                'email_id': e_id
                # 'message': 'No message was found in the email.'
            }

# convert the json to a pandas dataframe with columns of subject, from, and message
df_extra = pd.DataFrame(emails).T
df = pd.concat([df, df_extra], axis=0)


# Export the dataframe to an excel file with the creation time in the name 
now = datetime.datetime.now()

# format as a string
# filename = now.strftime("emails_%Y-%m-%d_%H-%M-%S.xlsx")
# filename = 'emails.xlsx'

df.to_excel(file_name)

end = datetime.datetime.now()
# strip the microseconds
process_time = (end - start).seconds
print(f'Processing {no_of_emails} emails took: {process_time} seconds')

# Close the connection
mail.close()